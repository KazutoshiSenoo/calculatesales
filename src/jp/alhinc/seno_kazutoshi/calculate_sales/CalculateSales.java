package jp.alhinc.seno_kazutoshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String [] args) {
		//コマンドライン引数でファイルのデータを読み込む
		HashMap<String, String> branchNames = new HashMap<>();//支店コード:支店名
		HashMap<String, Long> branchSales = new HashMap<>();//支店コード:売り上げ金額初期値（0円）
		BufferedReader br = null;
		try {
			try {
				br = new BufferedReader(new FileReader(new File(args[0], "branch.lst")));

				//読みこんだデータ出力
				String branch;
				while((branch = br.readLine()) != null) {
					String[] branchDefine = branch.split(",",0);
					//支店定義ファイルのフォーマットが不正な場合のエラー処理
					if (!branchDefine[0].matches("^[0-9]{3}$") || branchDefine.length != 2){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					//HashMapで支店コードと支店名を紐付ける
					branchNames.put(branchDefine[0], branchDefine[1]);
					branchSales.put(branchDefine[0], 0L);
				}
			//支店定義ファイルが存在しない場合のエラー処理
			} catch(IOException e) {
				System.out.println("支店定義ファイルが存在しません。");
				return;
			} finally {
				if(br != null) {
					br.close();
				}
				//拡張子がrcd、かつ8桁の数字のファイルを検索
				//Filefilterメソッド作成
			}

			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file2, String fileEnd){
					return fileEnd.matches("^\\d{8}.rcd$");
				}
			};

			//売上ファイルの配列作成
			File[] files = new File(args[0]).listFiles(filter);
			Arrays.sort(files);

			//連番でなかったらエラーを出す処理
			//一番小さい数
			Integer fileNumberMin = Integer.parseInt(files[0].getName().split("\\.",0)[0]);
			//一番大きい数
			Integer fileNumberMax = Integer.parseInt(files[files.length-1].getName().split("\\.",0)[0]);

			if(fileNumberMax - fileNumberMin != files.length - 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			//売上ファイル読み込み
			for(int i=0; i<files.length; ++i){
				String fileLetter = files[i].getName();
				BufferedReader br2 = null;

				try {

					br2 = new BufferedReader(new FileReader(files[i]));
					String branchCode = br2.readLine();
					Long rcdSales = Long.parseLong(br2.readLine());

					//支店に該当がない場合のエラー処理
					if(branchSales.get(branchCode)==null){
						System.out.println(fileLetter + "の支店コードが不正です");
						return;
					}

					//売上ファイルのフォーマット不正エラー処理
					if(br2.readLine() != null){
						System.out.println(fileLetter +"のフォーマットが不正です");
						return;
					}

					Long sum = branchSales.get(branchCode) + rcdSales;
					//合計金額が10桁超えたら処理終了のエラー処理
					if(sum.toString().length() > 10){
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					branchSales.put(branchCode, branchSales.get(branchCode) + rcdSales);

				} catch(IOException e) {
					System.out.println("エラーが発生しました");
				} finally {
					if(br2 != null) {
						br2.close();
					}
				}
			}
			//ここからbranch.outに出力する指示
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(args[0], "branch.out")));
				for (Entry<String,String> entry : branchNames.entrySet()){
					bw.write(entry.getKey() + "," + entry.getValue() + "," + branchSales.get(entry.getKey()));
					bw.newLine();
				}

				bw.close();
			} catch (IOException e) {
				System.out.println(e);
			}
			//予期せぬエラーが発生したときのエラー処理
		} catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}
